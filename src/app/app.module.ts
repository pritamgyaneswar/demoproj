import { OtpPage } from './../pages/otp/otp';
import { LoginPage } from './../pages/login/login';
import { httpService } from './../providers/customHttpService';
import { LoaderPage } from './../pages/loader/loader';
import { ForgotPasswordPage } from './../pages/forgot-password/forgot-password';
import { SignupPage } from './../pages/signup/signup';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import {WelcomePage} from '../pages/welcome/welcome';

import {PropertyService} from "../providers/property-service-mock";
import {BrokerService} from "../providers/broker-service-mock";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StarRatingModule } from 'ionic3-star-rating';
import { TabsPage } from '../pages/tabs/tabs';
import { HTTP } from '@ionic-native/http';

@NgModule({
  declarations: [
    MyApp,
    WelcomePage,
    TabsPage,
    SignupPage,
    ForgotPasswordPage,
    LoaderPage,
    LoginPage,
    OtpPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    StarRatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    WelcomePage,
    TabsPage,
    SignupPage,
    ForgotPasswordPage,
    LoaderPage,
    LoginPage,
    OtpPage
  ],
  providers: [
    HTTP,
    httpService,
    StatusBar,
    SplashScreen,
    PropertyService,
    BrokerService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
