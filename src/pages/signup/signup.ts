import { LoginPage } from "./../login/login";
import { environment } from "./../../app/environment";
import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  MenuController,
  ModalController
} from "ionic-angular";
import * as $ from "jquery";
import { LoaderPage } from "../loader/loader";
import { HTTP } from "@ionic-native/http";
// import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  error: string;
  // profile_image;
  email;
  password1;
  password2;
  fname;
  lname;
  phone;
  gender;
  register_as;
  customer_id;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HTTP,
    public Menu: MenuController,
    public modalCtrl: ModalController
  ) {
    // this.profile_image;
    this.email;
    this.password1;
    this.password2;
    this.fname;
    this.lname;
    this.phone;
    this.gender;
    this.register_as;
    this.customer_id;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad SignupPage");
    this.Menu.enable(false);
  }
  doSignUp() {
    //  this.navCtrl.push(SigninPage);
    let profileModal = this.modalCtrl.create(LoaderPage);
    $("#btn-register").addClass("highlight");
    if (
      this.email != null ||
      this.password1 != null ||
      this.password2 != null ||
      this.fname != null ||
      this.lname != null ||
      this.phone != null ||
      this.gender != null ||
      this.register_as != null
    ) {
      let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      if (this.register_as) {
        if (regExp.test(this.email)) {
          if (this.gender) {
            regExp = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
            if (regExp.test(this.phone)) {
              regExp = /[a-zA-Z ]*/;

              if (regExp.test(this.fname) && regExp.test(this.lname)) {
                if (this.password1 == this.password2) {
                  // let fd = new FormData();
                  // fd.append('profile_image', this.profile_image);
                  // fd.append('email', this.email);
                  // fd.append('fname', this.fname);
                  // fd.append('lname', this.lname);
                  // fd.append('phone', this.phone);
                  // fd.append('gender', this.gender);
                  // fd.append('password', this.password1);
                  // console.log('this.profile_image',this.profile_image);
                  profileModal.present();
                  this.http
                    .post(
                      environment.API_URL + "registration",
                      {
                        // 'profile_image': this.profile_image.name,
                        email: this.email,
                        fname: this.fname,
                        lname: this.lname,
                        customer_id: this.customer_id,
                        phone: this.phone,
                        gender: this.gender,
                        password: this.password1,
                        register_as: this.register_as,
                        device_token: "dhshdshshue"
                      },
                      {}
                    )
                    .then(
                      data => {
                        console.log(data);
                        if (data) {
                          this.email = "";
                          this.password1 = "";
                          this.password2 = "";
                          this.fname = "";
                          this.lname = "";
                          this.phone = "";
                          this.gender = "";
                          this.customer_id = "";
                        }
                        profileModal.dismiss();
                        this.navCtrl.setRoot(LoginPage);
                      },
                      error => {
                        console.log("error", error);
                      }
                    );
                } else {
                  this.error =
                    "you entered password and reenter password both are different";
                  this.showPopup();
                }
              } else {
                this.error = "enter valid name fname &lname";
                this.showPopup();
              }
            } else {
              this.error = "enter a valid phone number";
              this.showPopup();
            }
          } else {
            this.error = "enter a valid gender";
            this.showPopup();
          }
        } else {
          this.error = "enter a valid email address";
          this.showPopup();
        }
      } else {
        this.error = "enter a valid register option";
        this.showPopup();
      }
    } else {
      this.error = "enter all field for registration";
      this.showPopup();
    }
  }
  openSignInPage() {
    this.navCtrl.setRoot(LoginPage);
  }
  // changePic(event) {
  //   this.profile_image = event.srcElement.files[0];
  // }

  showPopup() {
    console.log("show Popup");
    $(".error_popup").show();
  }

  dismissPopup() {
    $(".error_popup").hide("slow");
  }
}
