import {Component} from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'page-welcome',
    templateUrl: 'welcome.html'
})
export class WelcomePage {
    brokers: Array<any>;

    constructor(public navCtrl: NavController) {
    }
}
