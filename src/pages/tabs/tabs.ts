import { LoginPage } from './../login/login';
import { WelcomePage } from './../welcome/welcome';
import { Component } from '@angular/core';
import {ViewChild} from '@angular/core';

import { Tabs, MenuController } from 'ionic-angular';

@Component({
  selector: 'app-tab',
  templateUrl: 'tabs.html'
})
export class TabsPage {

@ViewChild('myTabs') tabRef: Tabs;

ionViewDidEnter() {
  this.tabRef.select(0);
 }

  postTask = LoginPage;
  myTask = '';
  browseTask =  WelcomePage;

  constructor(public menuCtrl: MenuController) {

  }
  presentPopover(){
    this.menuCtrl.toggle();
  }
}
  
   