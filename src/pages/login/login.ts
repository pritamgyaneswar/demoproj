import { WelcomePage } from "./../welcome/welcome";
import { SignupPage } from "./../signup/signup";
import { ForgotPasswordPage } from "./../forgot-password/forgot-password";
import { LoaderPage } from "./../loader/loader";
import { environment } from "./../../app/environment";
import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  MenuController,
  ModalController
} from "ionic-angular";
// import { authenticationService } from "../../providers/authenticationService";
import { HTTP } from "@ionic-native/http";
import * as $ from "jquery";

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  login;
  email;
  password;
  login_as;
  pattern;
  error;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    // public auth: authenticationService,
    public http: HTTP,
    public Menu: MenuController,
    public modalCtrl: ModalController
  ) {
    if (localStorage.getItem("token")) {
      this.navCtrl.setRoot(WelcomePage);
    }
    this.email;
    this.password;
    this.login_as;
    this.pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$";
    this.error;
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad SigninPage");
    this.Menu.enable(false);
  }
  doLogin() {
    let profileModal = this.modalCtrl.create(LoaderPage);
    if (this.email != null && this.password != null) {
      // let regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      // if (regExp.test(this.email)) {
      profileModal.present();
      let headers = new Headers({ "Content-Type": "application/json" });

      this.http
        .post(
          environment.API_URL + "auth/login",
          {
            loginId: this.email,
            password: this.password
          },
          { 'headers': JSON.stringify(headers) }
        )
        // this.auth.login(this.email, this.password)
        .then(
          (response: any) => {
            console.log("response", response);
            let parsedResponse = JSON.parse(response.data);
            localStorage.setItem("token", parsedResponse.u_token);
            this.Menu.enable(true);
            profileModal.dismiss();
            this.navCtrl.setRoot(WelcomePage);
          },
          error => {
            console.log(error, "error");
            if (error.status == 401) {
              profileModal.dismiss();
              console.log(error.status);
              this.error =
                "you have enter a wrong credential for login \n try again";
              this.showPopup();
              this.email = null;
              this.password = null;
            } else {
              console.log(error, "error");
              profileModal.dismiss();
              this.error = "chek your network connection \n try again";
              this.showPopup();
              this.email = null;
              this.password = null;
            }
          }
        );
      // } else {
      //   this.error = 'enter a valid email address';
      //   this.showPopup();
      //   this.email = null;
      //   this.password = null;
      //   console.log(this.error);
      // }
    } else {
      this.error = "enter a username, password";
      this.showPopup();
      this.email = null;
      this.password = null;
      console.log(this.error);
    }
  }

  openSignUpPage() {
    this.navCtrl.setRoot(SignupPage);
  }
  openForgotPasswordPage() {
    this.navCtrl.setRoot(ForgotPasswordPage);
  }
  showPopup() {
    console.log("show Popup");
    $(".error_popup").show();
  }

  dismissPopup() {
    $(".error_popup").hide("slow");
  }
}
