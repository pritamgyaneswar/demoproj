import { LoginPage } from "./../login/login";
import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  MenuController,
  ModalController
} from "ionic-angular";
import { httpService } from "../../providers/customHttpService";
// import { Http, RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
import * as $ from "jquery";
import { LoaderPage } from "../loader/loader";

@Component({
  selector: "page-forgot-password",
  templateUrl: "forgot-password.html"
})
export class ForgotPasswordPage {
  flag: number;
  error: string;
  email: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public Menu: MenuController,
    public Http: httpService,
    public modalCtrl: ModalController
  ) {
    this.email;
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad ForgotPasswordPage");
    this.Menu.enable(false);
  }
  opnpagecode() {
    let profileModal = this.modalCtrl.create(LoaderPage);
    if (this.email) {
      profileModal.present();
      this.Http.forgottpassword(this.email).then(
        (result: any) => {
          console.log(result);
          this.error = result.msg;
          profileModal.dismiss();
          this.showPopup(1);
        },
        error => {
          this.email = null;
          profileModal.dismiss();
          this.error = "you enter a wrong email tray again";
          console.log(this.error);
          this.showPopup(0);
          console.log("try again");
        }
      ).catch();
    } else {
      this.error = "enter a valid email";
      this.showPopup(0);
    }
  }
  openSignInPage() {
    this.navCtrl.setRoot(LoginPage);
  }
  showPopup(a) {
    console.log("show Popup");
    this.flag = a;
    $(".error_popup").show();
  }

  dismissPopup() {
    if (this.flag == 1) {
      $(".error_popup").hide("slow");
      this.navCtrl.setRoot(LoginPage);
    } else {
      $(".error_popup").hide("slow");
    }
  }
}
