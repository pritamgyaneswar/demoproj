import { environment } from './../app/environment';
import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';

@Injectable()
export class authenticationService {

    constructor(private http: HTTP) {

    }

    login(username: string, password: string) {
        
        let url = environment.API_URL + 'login';
        let formData = new FormData();
        formData.append('email', username);
        formData.append('password', password);
        // return this.http.post(url, formData, { headers: headers });
        return this.http.post(url, formData, {'Authorization': localStorage.getItem('token')})

    }


}
