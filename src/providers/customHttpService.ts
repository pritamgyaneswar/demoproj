import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Headers } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { environment } from '../app/environment';

@Injectable()

export class httpService {
    [x: string]: any;
    private baseUrl = environment.API_URL;
    private token: string = '';

    // private serviceList;
    private fbToken;
    // private Userid;
    // private userEmail;
    private categary_ID;
    options: any;
    constructor(private http: HTTP) {

    }

    setFbToken(fbToken) {
        this.fbToken = fbToken;
        console.log(this.fbToken);
    }

    getFbToken() {
        return this.fbToken;
    }

    setToken(token) {
        if (token == null) {
            this.token = null
        } else {
            this.token = token;
        }
        localStorage.setItem('token', this.token);
    }

    getToken() {
        return this.token = localStorage.getItem('token');
    }

    getServiceList() {
        console.log('get service list');
        let url = 'service';
        let data = "";
        url = this.baseUrl + url;
        // let formData = new FormData();
        // var headers = new Headers();
        // this.headers.append('Authorization', localStorage.getItem('token'));
        return new Promise((resolve, reject) => {
            // this.http.get(url, data).subscribe((response: any) => {
            this.http.post(url, data, this.options).then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response)
                resolve(response.data);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }).catch( (error) => {
                console.log('Found Error', error);
            });
        })
    }

    getCategaryList() {
        let url = 'category';
        let data = "no data";
        url = this.baseUrl + url;
        // let formData = new FormData();
        // var headers = new Headers();
        // headers.append('Authorization', localStorage.getItem('token'));
        console.log('categary api');
        console.log(this.token);
        // return new Promise((resolve, reject) => {
            // this.http.get(url, data).subscribe((response: any) => {
            this.http.post(url, {}, {'Authorization': localStorage.getItem('token')});
            // .then((response: any) => {
            //     console.log(response.status)
            //     response = response.json();
            //     console.log(response)
            //     resolve(response.data);
            //     if (response.code == 200) {
            //         console.log('response.data',response.data)
            //     } else {
            //         console.log('error')
            //         reject(response);
            //     }
            // }), (error) => {
            //     reject(error);
            // };
        // })
    }

    getUserData() {
        let url = 'userdata';
        let data = "no data";
        url = this.baseUrl + url;
        // let formData = new FormData();
        var headers = new Headers();
        headers.append('authorization', this.token);
        return new Promise((resolve, reject) => {
            this.http.post(url, data, {'Authorization': localStorage.getItem('token')}).then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response)
                resolve(response.data);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }

    facebookUserData(userdata, imgurl) {
        let url = 'facebook';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        formData.append('profile_image', imgurl);
        formData.append('email', userdata.email);
        formData.append('fname', userdata.first_name);
        formData.append('lname', userdata.last_name);
        formData.append('gender', userdata.gender);
        console.log(formData);
        return new Promise((resolve, reject) => {
            this.http.post(url, formData, {'Authorization': localStorage.getItem('token')})
                .then((response: any) => {
                    if (response.status == 200) {
                        console.log('hello from succsses');
                        response = response.json();
                        console.log(response.token);
                        this.setToken(response.token);
                        resolve(response.data);
                    } else {
                        console.log('hello from else')
                        console.log('error')
                        console.error(response);
                        alert("try again some error ouccors");
                    }
                }), (error) => {
                    console.log('hello from main error')
                    reject(error);
                };
        })
    }

    changePassword(oldpsw, newpsw) {
        let url = 'changepassword';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        formData.append("oldpass", oldpsw);
        formData.append("newpass", newpsw);
        headers.append('authorization', this.token);
        return new Promise((resolve, reject) => {
            this.http.post(url, formData, {'Authorization': localStorage.getItem('token')})
            .then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response)
                resolve(response.data);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }

    getUserPoint() {
        let url = 'userpoint';
        let data = "no data";
        url = this.baseUrl + url;
        // let formData = new FormData();
        var headers = new Headers();
        console.log(this.token);
        headers.append('authorization', this.token);
        return new Promise((resolve, reject) => {
            this.http.get(url, data, {'Authorization': localStorage.getItem('token')}).then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response.data)
                resolve(response.data);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }

    getTimeSlot(service_id, date) {
        let url = 'timeslot';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        console.log(this.token);
        headers.append('authorization', this.token);
        formData.append('appointment_date', date);
        formData.append('service_id', service_id);
        console.log(formData);
        return new Promise((resolve, reject) => {
            // this.http.post(url, formData, { headers: headers }).subscribe((response: any) => {
            this.http.get(url, {}, {'Authorization': localStorage.getItem('token')}).then((response: any) => {

                if (response.status == 200) {
                    console.log(response)
                    console.log(response.status)
                    response = response.json();
                    console.log(response)
                    resolve(response.data);
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }

    post(url, data: FormData, { headers: headers }): Promise<any> {
        if (this.token) {
            this.headers.append('authorization', this.token);
        }
        url = this.baseUrl + url;

        return new Promise((resolve, reject) => {
            this.http.post(url, data, { headers: this.Headers }).then((response: any) => {
                console.log(response.status)
                response = response.json();
                resolve(response);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }, (error) => {
                reject(error);
            });
        });

    }

    setCategaryId(id) {
        this.categary_ID = id;
    }

    getCategaryID() {
        return this.categary_ID;
    }

    serviceByCategary(id) {
        let url = 'category_service';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        headers.append('authorization', this.token);
        formData.append('id', id);
        
        return new Promise((resolve, reject) => {
            // this.http.post(url, formData, { headers: headers }).subscribe((response: any) => {
            this.http.get(url, {}, {}).then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response)
                resolve(response.data);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }

    bookAppoinment(service_id, appointment_date, appointment_time,payment_status) {
        let url = 'appointment';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        
        headers.append('authorization', this.token);
        formData.append('service_id', service_id);
        formData.append('payment_status',payment_status);
        formData.append('appointment_date', appointment_date);
        formData.append('appointment_time', appointment_time);

        return new Promise((resolve, reject) => {
            this.http.post(url, formData, { headers: headers })
            // this.http.get(url)
                .then((response: any) => {
                    if (response.status == 200) {
                        console.log(response)
                        console.log(response.status)
                        response = response.json();
                        console.log(response)
                        resolve(response.data);
                    } else {
                        response = response.json();
                        console.error(response);
                        reject(response)
                    }
                }), (error) => {
                    error = error.json();
                    reject(error);
                };
        })
    }

    forgottpassword(email) {
        let url = 'forgotpassword';
        // let data = "no data";
        url = this.baseUrl + url;
        let formData = new FormData();
        var headers = new Headers();
        formData.append("email", email);
        return new Promise((resolve, reject) => {
            this.http.post(url, formData, { headers: headers })
                .then((response: any) => {
                    response = response.json();
                    resolve(response);
                }, (error) => {
                    error = error.json();
                    alert(error.msg)
                    reject(error);
                });
        })
    }

    setLocalToken(Token) {
        this.token = Token;
        console.log('set local token');
        console.log(this.token);
    }

    getGallary() {
        let url = 'gallary';
        let data = "no data";
        url = this.baseUrl + url;
        // let formData = new FormData();
        var headers = new Headers();
        headers.append('authorization', this.token);
        console.log('categary api');
        console.log(this.token);
        return new Promise((resolve, reject) => {
            this.http.post(url, data, { headers: headers }).then((response: any) => {
                console.log(response.status)
                response = response.json();
                console.log(response)
                resolve(response.gallary);
                if (response.code == 200) {
                } else {
                    console.log('error')
                    reject(response);
                }
            }), (error) => {
                reject(error);
            };
        })
    }
}
