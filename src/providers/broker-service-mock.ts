import {Injectable} from '@angular/core';
import brokers from './mock-brokers';

@Injectable()
export class BrokerService {

    findAll() {
        return Promise.resolve(brokers);
    }

    findById(id) {
        return Promise.resolve(brokers[id - 1]);
    }

    findByName(searchKey: string) {
        let key: string = searchKey.toUpperCase();
        return Promise.resolve(brokers.filter((broker: any) =>
            (broker.name +  ' ' +broker.title +  ' ' + broker.phone + ' ' + broker.picture).toUpperCase().indexOf(key) > -1));
      }
}
